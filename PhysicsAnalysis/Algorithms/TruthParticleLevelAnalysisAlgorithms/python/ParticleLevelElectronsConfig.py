# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

from AnalysisAlgorithmsConfig.ConfigBlock import ConfigBlock


class ParticleLevelElectronsBlock(ConfigBlock):
    """ConfigBlock for particle-level truth electrons"""

    def __init__(self):
        super(ParticleLevelElectronsBlock, self).__init__()
        self.addOption('containerName', 'TruthElectrons', type=str,
                       info='the name of the input truth electrons container')
        self.addOption('selectionName', '', type=str,
                       info='the name of the selection to create. The default is "",'
                       ' which applies the selection to all truth electrons.')
        self.addOption('isolated', True, type=bool,
                       info='select only truth electrons that are isolated.')
        self.addOption('notFromTau', True, type=bool,
                       info='select only truth electrons that did not orginate '
                       'from a tau decay.')
        # Always skip on data
        self.setOptionValue('skipOnData', True)

    def makeAlgs(self, config):
        config.setSourceName (self.containerName, self.containerName)

        # decorate the charge so we can save it later
        alg = config.createAlgorithm('CP::ParticleLevelChargeDecoratorAlg',
                                     'ParticleLevelChargeDecoratorElectrons' + self.selectionName,
                                     reentrant=True)
        alg.particles = self.containerName

        # check for prompt isolation and possible origin from tau decays
        alg = config.createAlgorithm('CP::ParticleLevelIsolationAlg',
                                     'ParticleLevelIsolationElectrons' + self.selectionName,
                                     reentrant=True)
        alg.particles    = self.containerName
        alg.isolation    = 'isIsolated' + self.selectionName if self.isolated else 'isIsolatedButNotRequired' + self.selectionName
        alg.notTauOrigin = 'notFromTau' + self.selectionName if self.notFromTau else 'notFromTauButNotRequired' + self.selectionName
        alg.checkType    = 'IsoElectron'

        if self.isolated:
            config.addSelection (self.containerName, self.selectionName, alg.isolation+',as_char')
        if self.notFromTau:
            config.addSelection (self.containerName, self.selectionName, alg.notTauOrigin+',as_char')

        # output branches to be scheduled only once
        if ParticleLevelElectronsBlock.get_instance_count() == 1:
            outputVars = [
                ['pt_dressed', 'pt'],
                ['eta_dressed', 'eta'],
                ['phi_dressed', 'phi'],
                ['e_dressed', 'e'],
                ['charge', 'charge'],
                ['classifierParticleType', 'type'],
                ['classifierParticleOrigin', 'origin'],
            ]
            for decoration, branch in outputVars:
                config.addOutputVar (self.containerName, decoration, branch, noSys=True)
