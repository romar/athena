/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#ifndef RNTCOLLECTIONQUERY_H
#define RNTCOLLECTIONQUERY_H

#include "CoralBase/AttributeList.h"

#include "AthenaBaseComps/AthMessaging.h"

#include "CollectionBase/TokenList.h"
#include "CollectionBase/ICollectionQuery.h"
#include "CollectionBase/ICollectionDescription.h"
#include "CollectionBase/ICollectionCursor.h"

#include "RNTCollectionCursor.h"

#include <set>

namespace pool::RootCollection {

   /** 
    * @class RNTCollectionQuery RNTCollectionQuery.h Rootcollection/RNTCollectionQuery.h
    *
    * An interface used to query an RNTuple collection.
    */
   class RNTCollectionQuery : public ICollectionQuery, public AthMessaging
   {
   public:
      /// Constructor
      RNTCollectionQuery( const pool::ICollectionDescription& description, RNTupleReader *reader );
    
      /// Destructor
      virtual ~RNTCollectionQuery();

      /**
       * Adds a column to the query select list.
       *
       * @param columnName Name of Attribute column to select.
       */
      virtual void addToOutputList( const std::string& columnName );

      /**
       * Adds one or more columns to the query select list.
       *
       * @param columnNames Names of Attribute columns to select.
       */
      virtual void addToOutputList( const std::vector<std::string>& columnNames );

      /// Adds all Attribute columns to the query select list.
      virtual void selectAllAttributes();

      /// Adds all Token columns to the query select list.
      virtual void selectAllTokens();

      /// Adds all Token and Attribute columns to the query select list.
      virtual void selectAll();

      /**
       * Sets the query.
       * the query can be
       * constructed in fragments by repeated calls to `setCondition'. 
       *
       * @param whereClause The query.
       * @param attributeBindData  - unused
       * @param tokenBindData  - unused
       */
      virtual void setCondition( const std::string& whereClause,
                                 coral::AttributeList* attributeBindData = 0,
                                 TokenList* tokenBindData = 0 ) ;

      /// Returns the where clause of the query.
      virtual const std::string& whereClause() const;

      /**
       * Sets the cache size used to store the query result.
       *
       * @param Number of rows stored in cache before cache is flushed.
       */
      virtual void setRowCacheSize( int rowCacheSize );

      /// Processes the query and returns a cursor over the query result.
      virtual pool::ICollectionCursor& execute();

      /**
       * Tell the query to not include the primary event reference
       * in the result by default (it can still beselected manually)
       *
       * @param skip if true (the default) then skip the primary event reference
       */
      virtual void skipEventReference( bool = true );

   protected:

      void                addToTokenOutputList( const std::string& columnName );

      void                addToAttributeOutputList( const std::string& columnName );
        
        
      const ICollectionDescription    &m_description;
      RNTupleReader                  *m_reader {nullptr};   // owned by the Collection

      RNTCollectionCursor            *m_cursor {nullptr};

      std::string                     m_whereClause;

      pool::TokenList                 m_outputTokenList;
      coral::AttributeList            m_outputAttributeList;

      std::set< std::string >         m_selectedColumnNames;

      /// If false, the primary event reference is added always to the query result
      bool                            m_skipEventRef; 
   };

} // end namespace 

#endif
