// Dear emacs, this is -*- c++ -*-
/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRIGGER_VERSIONS_BYTESTREAMAUXCONTAINER_V1_ICC
#define XAODTRIGGER_VERSIONS_BYTESTREAMAUXCONTAINER_V1_ICC

// System include(s):
#include <iostream>

// EDM include(s):
#include "AthContainers/AuxTypeRegistry.h"
#include "xAODCore/tools/AuxPersVector.h"
#include "xAODCore/tools/AuxVariable.h"

namespace xAOD {

   template< typename T >
   ByteStreamAuxContainer_v1::auxid_t
   ByteStreamAuxContainer_v1::getAuxID( const std::string& name,
                                        std::vector< T >& /*vec*/,
                                        SG::AuxVarFlags flags) {

      return SG::AuxTypeRegistry::instance().template getAuxID< T >( name, "", flags );
   }

   /// The user is expected to use this function inside the constructor of
   /// the derived class.
   ///
   /// @param name The name of the variable. Same as the C++ variable's name.
   /// @param vec A reference to the auxiliary variable inside the object
   ///
   template< typename T >
   void ByteStreamAuxContainer_v1::regAuxVar( const auxid_t auxid,
                                              const std::string& name,
                                              std::vector< T >& vec ) {

      // Make sure that the internal vector is big enough:
      if( m_staticVecs.size() <= auxid ) {
         m_staticVecs.resize( auxid + 1 );
      }

      // Check if this variable name was already registered:
      if( m_staticVecs[ auxid ] ) {
         std::cerr << "WARNING xAOD::AuxContainerBase::regAuxVec "
                   << "Re-registering variable with name \""
                   << name << "\"" << std::endl;
         delete m_staticVecs[ auxid ];
      }

      const SG::AuxTypeRegistry& r = SG::AuxTypeRegistry::instance();
      if (r.isLinked(auxid)) {
         throw std::runtime_error ("ERROR xAOD::ByteStreamAuxContainer_v1 doesn't implement linked variables");
      }

      // Register the variable:
      m_staticVecs[ auxid ] = new AuxPersVector< T >( auxid, vec, false, this );

      // Remember that we are now handling this variable:
      m_auxids.insert( auxid );

      return;
   }

} // namespace xAOD

#endif // XAODTRIGGER_VERSIONS_BYTESTREAMAUXCONTAINER_V1_ICC
